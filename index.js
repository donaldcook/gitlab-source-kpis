const { Parser } = require('json2csv');

const { checkBranch } = require('./lib/checkBranch');
const { commitStats, redMasterStats } = require('./lib/exporter');

const { getAllReports } = require('./lib/teamMemberDefinitions');

exports.updateStats = async (req, res) => {
  const result = await checkBranch('gitlab-org%2Fgitlab', 0, 10);
  res.status(200).send(result);
};

exports.commitStats = async (req, res) => {
  const result = await commitStats();
  res.status(200).send(result);
};

exports.commitStatsCSV = async (req, res) => {
  const result = await commitStats();

  try {
    const parser = new Parser({});
    const csv = parser.parse(result);

    res.setHeader('Content-disposition', 'attachment; filename=GitLabCodeStats.csv');
    res.setHeader('Content-type', 'text/csv');

    res.status(200).send(csv);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
};

exports.redMasterStats = async (req, res) => {
  const result = await redMasterStats();
  res.status(200).send(result);
};

exports.redMasterStatsCSV = async (req, res) => {
  const result = await redMasterStats();

  try {
    const parser = new Parser({});
    const csv = parser.parse(result);

    res.setHeader('Content-disposition', 'attachment; filename=GitLabRedMasterStats.csv');
    res.setHeader('Content-type', 'text/csv');

    res.status(200).send(csv);
  } catch (err) {
    console.error(err);
    res.status(500).send(err);
  }
};

exports.developmentTeamMembers = async (req, res) => {
  const reports = await getAllReports();
  res.status(200).send(reports);
};
