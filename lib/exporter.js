const Firestore = require('@google-cloud/firestore');

const PROJECTID = 'gl-source-kpi-collector';
const COLLECTION_NAME = 'commits';
const firestore = new Firestore({
  projectId: PROJECTID,
});

exports.commitStats = async function() {
  const commitDocSnapshots = await firestore
    .collection(COLLECTION_NAME)
    .where('greenPipeline', '==', true)
    .get();

  const result = [];
  const addedDates = [];
  let docCount = 0;

  commitDocSnapshots.forEach(function(doc) {
    const selectedDoc = doc.data();
    if (selectedDoc.jestCoverage || selectedDoc.karmaCoverage || selectedDoc.backgroundCoverage) {
      delete selectedDoc.id;
      delete selectedDoc.greenPipeline;
      selectedDoc.commitDate = new Date(selectedDoc.commitDate).toLocaleDateString('en-US');
      // Only add one line per date to save CSV size as we are anyhow only showing 1 point per day

      if (!addedDates.includes(selectedDoc.commitDate)) {
        addedDates.push(selectedDoc.commitDate);
        result.push(selectedDoc);
      }
    }
    docCount++;
  });

  console.log('Total Count : ' + docCount);

  return result;
};

exports.redMasterStats = async function() {
  const commitDocSnapshots = await firestore
    .collection(COLLECTION_NAME)
    .where('greenPipeline', '==', false)
    .get();

  const result = [];
  const addedDates = [];

  console.log(`${Object.keys(commitDocSnapshots).length} Commits`);

  commitDocSnapshots.forEach(function(doc) {
    const selectedDoc = doc.data();
    result.push({
      id: selectedDoc.id,
      date: new Date(selectedDoc.commitDate).toLocaleDateString('en-US'),
    });
  });

  return result;
};
