# GitLab Source KPI Collector

Collecting KPI's of our Projects in a Serverless application that is exposing data as Rest API/Posting to Periscope

## Setting it up locally

1. Create a `.env` file based on `example.env` with your personal GitLab API Key.
1. Get GCP configuration file and save it locally.
1. Set environment variable for GCP credentials file `export GOOGLE_APPLICATION_CREDENTIALS="[PATH]"`
1. `yarn run dev` to start the local dev Server which sets up the 3 endpoints locally.


`//localhost:8080/getStats` Will crawl through the latest 100 pages of commits + pipelines.

`//localhost:8080/getCommitStatsCSV` will download the stats as CSV.

`//localhost:8080/getRedMasterStatsCSV` will download a CSV list of all red masters.

## Deployment

CI Magic is automatically deploying it to GCP
